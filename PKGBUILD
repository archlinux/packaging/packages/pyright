# Maintainer: Daniel M. Capella <polyzen@archlinux.org>
# Contributor: Kaizhao Zhang <zhangkaizhao@gmail.com>

pkgname=pyright
pkgver=1.1.396
pkgrel=1
pkgdesc='Type checker for the Python language'
arch=(any)
url=https://microsoft.github.io/pyright
license=(MIT)
depends=(nodejs)
makedepends=(
  git
  npm
)
source=("git+https://github.com/microsoft/pyright.git#tag=$pkgver")
b2sums=('5692a5d43109e01cae63a32aff04945eb8a8b3678d262550e1899934cbae66e74b91b1770da2d3c6657e80f6e4e6d5a96c7a5c3a40e935d1e1cea23edad729fe')

prepare() {
  cd $pkgname
  npm ci
}

build() {
  cd $pkgname/packages/$pkgname
  npm run build
}

check() {
  cd $pkgname/packages/$pkgname-internal
  npm test
}

package() {
  local mod_dir=/usr/lib/node_modules/$pkgname

  install -d "$pkgdir"/{usr/bin,$mod_dir}
  ln -s $mod_dir/index.js "$pkgdir"/usr/bin/$pkgname
  ln -s $mod_dir/langserver.index.js "$pkgdir"/usr/bin/$pkgname-langserver

  cd $pkgname
  install -Dm644 -t "$pkgdir"/usr/share/doc/$pkgname README.md
  install -Dm644 -t "$pkgdir"/usr/share/licenses/$pkgname LICENSE.txt

  cd packages/$pkgname
  cp -r dist {,langserver.}index.js package.json "$pkgdir"/$mod_dir
}
